<?php

use Illuminate\Support\Facades\Route;

Route::prefix('api')->name('laravel-cluster.')->group(function () {
    // Dashboard Routes...
    Route::get('/dashboard', 'DashboardStatsController@index')->name('dashboard');

    // Inspector Routes...
    Route::get('/inspector', 'JobInspectorController@index')->name('inspector');

    // Queue Routes...
    Route::get('/queue', 'QueueMetricsController@index')->name('queue');

    // Job Routes...
    Route::get('/pending', 'PendingJobsController@index')->name('pending');
    Route::get('/pending/{clusterJob}', 'JobsController@show')->name('pending.show');
    Route::get('/completed', 'CompletedJobsController@index')->name('completed');
    Route::get('/completed/{clusterJob}', 'JobsController@show')->name('completed.show');
    Route::get('/failed', 'FailedJobsController@index')->name('failed');
    Route::get('/failed/{clusterJob}', 'JobsController@show')->name('failed.show');
    Route::get('/cancelled', 'CancelledJobsController@index')->name('cancelled');
    Route::get('/cancelled/{clusterJob}', 'JobsController@show')->name('cancelled.show');
    Route::delete('/cancel/{clusterJob}', 'CancelController@cancel')->name('cancel');
});

// Catch-all Route...
Route::get('/{view?}', 'HomeController@index')->where('view', '(.*)')->name('laravel-cluster.index');
