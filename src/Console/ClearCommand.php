<?php


namespace Dterumal\LaravelCluster\Console;


use Dterumal\LaravelCluster\Repositories\DatabaseJobRepository;
use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;

class ClearCommand extends Command
{
    use ConfirmableTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laravel-cluster:clear
                            {--force : Force the operation to run when in production}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete all the pending jobs of the cluster';

    /**
     * Execute the console command.
     *
     * @return int|null
     */
    public function handle(DatabaseJobRepository $jobRepository)
    {
        if (! $this->confirmToProceed()) {
            return 1;
        }
        $count = $jobRepository->purge();

        $this->line('<info>Cleared '.$count.' jobs from the cluster queue</info> ');

        return 0;
    }
}
