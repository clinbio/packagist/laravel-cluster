<?php

namespace Dterumal\LaravelCluster\Console;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;


class ClusterJobMakeCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'make:cluster-job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new cluster job class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'ClusterJob';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub(): string
    {
        return $this->option('callback')
            ? $this->resolveStubPath('/stubs/cluster-job.callback.stub')
            : $this->resolveStubPath('/stubs/cluster-job.stub');
    }

    /**
     * Resolve the fully-qualified path to the stub.
     *
     * @param  string  $stub
     * @return string
     */
    protected function resolveStubPath(string $stub)
    {
        return file_exists($customPath = $this->laravel->basePath(trim($stub, '/')))
            ? $customPath
            : __DIR__.$stub;
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Jobs';
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['callback', null, InputOption::VALUE_NONE, 'Indicates that job should contain extra logic based on events'],
        ];
    }

}
