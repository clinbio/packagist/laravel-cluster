<?php

namespace Dterumal\LaravelCluster\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laravel-cluster:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install all of the LaravelCluster resources';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->comment('Publishing LaravelCluster Service Provider...');
        $this->callSilent('vendor:publish', ['--tag' => 'laravel-cluster-provider']);

        $this->comment('Publishing LaravelCluster Assets...');
        $this->callSilent('vendor:publish', ['--tag' => 'laravel-cluster-assets']);

        $this->comment('Publishing LaravelCluster Configuration...');
        $this->callSilent('vendor:publish', ['--tag' => 'laravel-cluster-config']);

        $this->comment('Publishing LaravelCluster Migrations...');
        $this->callSilent('vendor:publish', ['--tag' => 'laravel-cluster-migrations']);

        $this->registerLaravelClusterServiceProvider();

        $this->info('LaravelCluster scaffolding installed successfully.');
    }

    /**
     * Register the LaravelCluster service provider in the application configuration file.
     *
     * @return void
     */
    protected function registerLaravelClusterServiceProvider()
    {
        $namespace = Str::replaceLast('\\', '', $this->laravel->getNamespace());

        $appConfig = file_get_contents(config_path('app.php'));

        if (Str::contains($appConfig, $namespace.'\\Providers\\LaravelClusterServiceProvider::class')) {
            return;
        }

        file_put_contents(config_path('app.php'), str_replace(
            "{$namespace}\\Providers\EventServiceProvider::class,".PHP_EOL,
            "{$namespace}\\Providers\EventServiceProvider::class,".PHP_EOL."        {$namespace}\Providers\LaravelClusterServiceProvider::class,".PHP_EOL,
            $appConfig
        ));

        file_put_contents(app_path('Providers/LaravelClusterServiceProvider.php'), str_replace(
            "namespace App\Providers;",
            "namespace {$namespace}\Providers;",
            file_get_contents(app_path('Providers/LaravelClusterServiceProvider.php'))
        ));
    }
}
