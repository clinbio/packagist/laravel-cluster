<?php

namespace Dterumal\LaravelCluster\Console;

use Dterumal\LaravelCluster\Contracts\ClusterInterface;
use Illuminate\Console\Command;

class MonitorClusterStatusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laravel-cluster:monitor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Monitor the cluster status and notify by mail if down';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        app(ClusterInterface::class)->monitorStatus();
    }
}
