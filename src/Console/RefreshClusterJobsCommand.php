<?php

namespace Dterumal\LaravelCluster\Console;

use Dterumal\LaravelCluster\Contracts\ClusterInterface;
use Illuminate\Console\Command;

class RefreshClusterJobsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laravel-cluster:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update jobs information in the database';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        app(ClusterInterface::class)->reloadInfo();
    }
}
