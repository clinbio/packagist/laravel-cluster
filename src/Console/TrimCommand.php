<?php

namespace Dterumal\LaravelCluster\Console;

use Dterumal\LaravelCluster\Repositories\DatabaseJobRepository;
use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;

class TrimCommand extends Command
{
    use ConfirmableTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laravel-cluster:trim';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trim all jobs according to configured minutes';

    /**
     * Execute the console command.
     *
     * @return int|null
     */
    public function handle(DatabaseJobRepository $jobRepository)
    {
        $jobRepository->trim();

        $this->line('<info>Trimmed jobs from the cluster table</info>');

        return 0;
    }
}
