<?php

namespace Dterumal\LaravelCluster\Http\Controllers;

use Dterumal\LaravelCluster\Contracts\JobRepository;
use Dterumal\LaravelCluster\Storage\JobModel;

class JobsController extends Controller
{
    /**
     * Returns a specific job information
     *
     * @param  JobModel  $clusterJob
     * @return array
     */
    public function show(JobModel $clusterJob): array
    {
        return [
            'job' => app(JobRepository::class)->find($clusterJob)
        ];
    }
}
