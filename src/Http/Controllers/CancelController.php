<?php

namespace Dterumal\LaravelCluster\Http\Controllers;

use Dterumal\LaravelCluster\Contracts\ClusterInterface;
use Dterumal\LaravelCluster\Storage\JobModel;
use Illuminate\Http\RedirectResponse;

class CancelController extends Controller
{
    /**
     * Cancel a specific job
     *
     * @param  JobModel  $clusterJob
     * @return array
     */
    public function cancel(JobModel $clusterJob): array
    {
        return [
                'status' => app(ClusterInterface::class)->cancel($clusterJob)
            ];
    }
}
