<?php

namespace Dterumal\LaravelCluster\Http\Controllers;

use Dterumal\LaravelCluster\Contracts\JobRepository;

class CancelledJobsController extends Controller
{

    /**
     * Returns the list of completed jobs
     *
     * @return array
     */
    public function index(): array
    {
        return [
            'jobs' => app(JobRepository::class)->cancelledJobs()
        ];
    }

}
