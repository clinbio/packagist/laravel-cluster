<?php

namespace Dterumal\LaravelCluster\Http\Controllers;

use Dterumal\LaravelCluster\Contracts\ClusterInterface;
use Dterumal\LaravelCluster\Contracts\MetricsRepository;

class DashboardStatsController extends Controller
{
    /**
     *  Get the key performance stats for the dashboard.
     *
     * @return array
     */
    public function index(): array
    {
        return [
            'totalJobs' => app(MetricsRepository::class)->count(),
            'jobsPastHour' => app(MetricsRepository::class)->countLastHour(),
            'failedJobs' => app(MetricsRepository::class)->countFailed(),
            'isOnline' => app(ClusterInterface::class)->status(),
            'nodes' => app(ClusterInterface::class)->nodes(),
        ];
    }
}
