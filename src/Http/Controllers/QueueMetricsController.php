<?php

namespace Dterumal\LaravelCluster\Http\Controllers;

use Dterumal\LaravelCluster\Contracts\ClusterInterface;
use Illuminate\Http\Request;

class QueueMetricsController extends Controller
{
    /**
     * Returns the dashboard with live data
     *
     * @return array
     */
    public function index(): array
    {
        return [
            'queues' => app(ClusterInterface::class)->queues()
        ];
    }
}
