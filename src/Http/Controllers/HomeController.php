<?php


namespace Dterumal\LaravelCluster\Http\Controllers;


use Dterumal\LaravelCluster\LaravelCluster;
use Illuminate\Support\Facades\App;

class HomeController extends Controller
{
    /**
     * Single page application catch-all route.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('laravel-cluster::layout', [
            'laravelClusterScriptVariables' => LaravelCluster::scriptVariables(),
        ]);
    }
}
