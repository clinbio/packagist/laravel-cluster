<?php

namespace Dterumal\LaravelCluster\Http\Controllers;

use Dterumal\LaravelCluster\Contracts\JobRepository;

class PendingJobsController extends Controller
{
    /**
     * Get all the failed jobs
     *
     * @return array
     */
    public function index(): array
    {
        $jobs = app(JobRepository::class)->pendingJobs();

        return [
            'jobs' => $jobs
        ];
    }
}
