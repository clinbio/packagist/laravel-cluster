<?php

namespace Dterumal\LaravelCluster\Http\Controllers;

use Dterumal\LaravelCluster\Contracts\ClusterInterface;
use Illuminate\Http\Request;

class JobInspectorController extends Controller
{
    /**
     * Returns cluster info of a job
     *
     * @param  Request  $request
     * @return array
     */
    public function index(Request $request): array
    {
        return [
            'information' => app(ClusterInterface::class)->info($request->input('jobId'))
        ];
    }
}
