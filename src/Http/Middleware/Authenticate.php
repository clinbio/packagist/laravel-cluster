<?php


namespace Dterumal\LaravelCluster\Http\Middleware;


use Dterumal\LaravelCluster\LaravelCluster;

class Authenticate
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return \Illuminate\Http\Response|null
     */
    public function handle($request, $next)
    {
        return LaravelCluster::check($request) ? $next($request) : abort(403);
    }
}

