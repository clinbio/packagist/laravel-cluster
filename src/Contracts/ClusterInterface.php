<?php

namespace Dterumal\LaravelCluster\Contracts;

use Dterumal\LaravelCluster\Storage\JobModel;

interface ClusterInterface
{
    /**
     * Returns status of the cluster
     *
     * @return bool
     */
    public function status(): bool;

    /**
     * Monitor the cluster status
     *
     * @return void
     */
    public function monitorStatus(): void;

    /**
     * Returns all the nodes
     *
     * @return array
     */
    public function nodes(): array;

    /**
     * Returns all the queues
     *
     * @return array
     */
    public function queues(): array;

    /**
     * Returns a specific cluster job information
     *
     * @param  string|null  $jobId
     * @return string|null
     */
    public function info(?string $jobId): ?string;

    /**
     * Reload jobs information
     *
     * @return void
     */
    public function reloadInfo(): void;

    /**
     * Dispatch the job
     *
     * @param  mixed  $job
     */
    public function run($job): void;
}
