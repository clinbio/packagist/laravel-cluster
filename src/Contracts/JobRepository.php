<?php


namespace Dterumal\LaravelCluster\Contracts;


use Dterumal\LaravelCluster\Storage\JobModel;
use Illuminate\Database\Eloquent\Collection;

interface JobRepository
{
    /**
     * Store the hob into storage
     *
     * @param $job
     * @param  string  $payload
     * @return JobModel
     */
    public function store($job, string $payload): JobModel;

    /**
     * Mark the job as queued
     *
     * @param  JobModel  $clusterJob
     * @param  string  $datetime
     * @return void
     */
    public function queued(JobModel $clusterJob, string $datetime): void;

    /**
     * Mark the job as failed
     *
     * @param  JobModel  $clusterJob
     * @param  string  $datetime
     * @return void
     */
    public function failed(JobModel $clusterJob, string $datetime): void;

    /**
     * Mark the job as cancelled
     *
     * @param  JobModel  $clusterJob
     * @param  string  $datetime
     * @return void
     */
    public function cancelled(JobModel $clusterJob, string $datetime): void;

    /**
     * Mark the job as completed
     *
     * @param  JobModel  $clusterJob
     * @param  string  $datetime
     * @return void
     */
    public function completed(JobModel $clusterJob, string $datetime): void;

    /**
     * Returns all the pending jobs
     *
     * @return Collection
     */
    public function pendingJobs(): Collection;

    /**
     * Returns all the completed jobs
     *
     * @return Collection
     */
    public function completedJobs(): Collection;

    /**
     * Returns all the failed jobs
     *
     * @return Collection
     */
    public function failedJobs(): Collection;

    /**
     * Returns all the failed jobs
     *
     * @return Collection
     */
    public function cancelledJobs(): Collection;

    /**
     * Returns a specific cluster job
     *
     * @param  JobModel  $clusterJob
     * @return JobModel
     */
    public function find(JobModel $clusterJob): JobModel;

    /**
     * Delete pending and reserved jobs for a queue.
     *
     * @return int
     */
    public function purge(): int;

    /**
     * Trim jobs based on configured minutes.
     *
     * @return void
     */
    public function trim(): void;
}
