<?php


namespace Dterumal\LaravelCluster\Contracts;


use Dterumal\LaravelCluster\Storage\JobModel;

interface ClusterWithCallbacks
{
    public function onFailure(JobModel $clusterJob): void;

    public function onSuccess(JobModel $clusterJob): void;

    public function onCancel(JobModel $clusterJob): void;
}
