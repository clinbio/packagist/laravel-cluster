<?php

namespace Dterumal\LaravelCluster\Contracts;

interface ClusterShouldQueue
{
    public function handle(): array;
}
