<?php


namespace Dterumal\LaravelCluster\Contracts;


interface MetricsRepository
{
    /**
     * Returns total of jobs
     *
     * @return int
     */
    public function count(): int;

    /**
     * Returns total of jobs of last hour
     *
     * @return int
     */
    public function countLastHour(): int;

    /**
     * Returns total of failed jobs
     *
     * @return int
     */
    public function countFailed(): int;
}
