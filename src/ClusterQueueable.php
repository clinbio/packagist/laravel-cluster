<?php

namespace Dterumal\LaravelCluster;

trait ClusterQueueable
{
    /**
     * The id of the job
     *
     * @var int
     */
    public int $id;

    /**
     * The name of the job.
     *
     * @var string
     */
    public string $name = 'default';

    /**
     * The name of the partition the job should be sent to.
     *
     * @var string
     */
    public string $partition = 'normal';

    /**
     * The number of nodes requested for the job.
     *
     * @var int|null
     */
    public ?int $nodes;

    /**
     * The number of task count requested for the job.
     *
     * @var int|null
     */
    public ?int $nTasks;

    /**
     * The number of processors per task requested for the job.
     *
     * @var int|null
     */
    public ?int $cpusPerTask;

    /**
     * The export path for the job.
     *
     * @var string
     */
    public string $exportPath = '/data';

    /**
     * The memory limit for the job.
     *
     * @var string
     */
    public string $memory = '1G';

    /**
     * The time limit for the job.
     *
     * @var string
     */
    public string $timeout = '00:01:00';

    /**
     * Set the name for the job.
     *
     * @param  string  $jobName
     * @return $this
     */
    public function name(string $jobName)
    {
        $this->name = $jobName;

        return $this;
    }

    /**
     * Set the number of nodes for the job.
     *
     * @param  int  $nodes
     * @return $this
     */
    public function nodes(int $nodes)
    {
        $this->nodes = $nodes;

        return $this;
    }

    /**
     * Set the number of task count for the job.
     *
     * @param  int  $nTasks
     * @return $this
     */
    public function nTasks(int $nTasks)
    {
        $this->nTasks = $nTasks;

        return $this;
    }

    /**
     * Set the number cpu per task required for the job.
     *
     * @param  int  $cpuPerTask
     * @return $this
     */
    public function cpusPerTask(int $cpuPerTask)
    {
        $this->cpusPerTask = $cpuPerTask;

        return $this;
    }

    /**
     * Set the memory limit for the job.
     *
     * @param  string  $limit
     * @return $this
     */
    public function memory(string $limit)
    {
        $this->memory = $limit;

        return $this;
    }

    /**
     * Set the time limit for the job.
     *
     * @param  string  $limit
     * @return $this
     */
    public function timeout(string $limit)
    {
        $this->timeout = $limit;

        return $this;
    }

    /**
     * Set the desired partition for the job.
     *
     * @param  string  $partition
     * @return $this
     */
    public function onPartition(string $partition)
    {
        $this->partition = $partition;

        return $this;
    }

    /**
     * Set the standard error path for the job.
     *
     * @param  string  $path
     * @return $this
     */
    public function exportTo(string $path)
    {
        $this->exportPath = $path;

        return $this;
    }
}
