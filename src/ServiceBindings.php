<?php


namespace Dterumal\LaravelCluster;


trait ServiceBindings
{
    /**
     * All of the service bindings for LaravelCluster.
     *
     * @var array
     */
    public $serviceBindings = [
        // General services...
        Contracts\ClusterInterface::class => Connectors\SlurmConnector::class,

        // Repository services...
        Contracts\MetricsRepository::class => Repositories\DatabaseMetricsRepository::class,
        Contracts\JobRepository::class => Repositories\DatabaseJobRepository::class
    ];
}
