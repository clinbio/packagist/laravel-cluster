<?php

namespace Dterumal\LaravelCluster;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Dterumal\LaravelCluster\LaravelCluster
 */
class LaravelClusterFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'laravel-cluster';
    }
}
