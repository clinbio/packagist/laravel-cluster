<?php


namespace Dterumal\LaravelCluster\Notifications;


use Dterumal\LaravelCluster\LaravelCluster;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Carbon;

class ClusterDownDetected extends Notification
{
    use Queueable;

    /**
     * The time of the incident.
     *
     * @var string
     */
    public string $datetime;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->datetime = Carbon::now()->toString();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return array_filter([
            LaravelCluster::$email ? 'mail' : null,
        ]);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->error()
            ->subject(config('app.name').': Cluster Down Detected')
            ->greeting('Oh no! Something needs your attention.')
            ->line(sprintf(
                'The cluster went down at %s.',
                $this->datetime
            ));
    }

    /**
     * The unique signature of the notification.
     *
     * @return string
     */
    public function signature()
    {
        return md5(config('app.name').$this->datetime);
    }
}
