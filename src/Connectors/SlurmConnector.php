<?php


namespace Dterumal\LaravelCluster\Connectors;

use Dterumal\LaravelCluster\Contracts\ClusterInterface;
use Dterumal\LaravelCluster\Contracts\JobRepository;
use Dterumal\LaravelCluster\Events\ClusterDownDetected;
use Dterumal\LaravelCluster\Events\ClusterUpDetected;
use Dterumal\LaravelCluster\Events\JobCancelled;
use Dterumal\LaravelCluster\Events\JobCompleted;
use Dterumal\LaravelCluster\Events\JobFailed;
use Dterumal\LaravelCluster\Events\JobQueued;
use Dterumal\LaravelCluster\Storage\JobModel;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class SlurmConnector implements ClusterInterface
{
    public function execute(array $commands, callable $onError = null)
    {
        $process = new Process($commands);

        $process->run();

        if (!$process->isSuccessful()) {
            if (!is_null($onError)) {
                return $onError();
            }

            throw new ProcessFailedException($process);
        }

        return $process->getOutput();
    }

    public function formatResults(string $output, array $overrideHeader = null)
    {
        $collection = collect(explode("\n", $output))
            ->filter()
            ->map(function ($line) {
                return str_getcsv($line, '|');
            });

        $header = collect($collection->first());

        if (!empty($overrideHeader)) {
            $header = collect($overrideHeader);
        }

        $rows = $collection->splice(1);

        return $rows->map(function ($row) use ($header) {
            return $header->mapWithKeys(function ($item, $key) use ($row) {
                $header = Str::remove(' ', Str::lower($item));
                return [$header => $row[$key]];
            });
        })->toArray();
    }

    public function mapEvents(Collection $collection)
    {
        return $collection->map(function ($job) {
            switch ($job['state']) {
                case 'COMPLETED':
                    $event = 'completed_at';
                    break;
                case 'CANCELLED':
                    $event = 'cancelled_at';
                    break;
                case 'OUT_OF_MEMORY':
                case 'NODE_FAIL':
                case 'STOPPED':
                case 'FAILED':
                    $event = 'failed_at';
                    break;
                case 'TIMEOUT':
                    $event = 'timed_out_at';
                    break;
                default:
                    $event = null;
                    break;
            }
            $info = [
                'id' => $job['id'],
                'queued_at' => $job['queued_at'],
            ];

            if ($event) {
                $info['datetime'] = $job['ended_at'];
                $info['event'] = $event;
            }

            return $info;
        });
    }

    public function triggerEvents(Collection $collection)
    {
        $collection->each(function ($job) {
            $clusterJob = JobModel::findOrFail($job['id']);
            event(new JobQueued($clusterJob, $job['queued_at']));
            if (array_key_exists('event', $job)) {
                if ($job['event'] === 'completed_at') {
                    event(new JobCompleted($clusterJob, $job['datetime']));
                } elseif ($job['event'] === 'failed_at' || $job['event'] === 'timed_out_at') {
                    event(new JobFailed($clusterJob, $job['datetime']));
                } elseif ($job['event'] === 'cancelled_at') {
                    event(new JobCancelled($clusterJob, $job['datetime']));
                }
            }
        });
    }

    /**
     * @inheritDoc
     */
    public function nodes(): array
    {
        $output = $this->execute([
            'sinfo',
            '--format=%all'
        ], fn() => []);

        if (empty($output)) {
            return [];
        }

        return $this->formatResults($output);
    }

    /**
     * @inheritDoc
     */
    public function info(?string $jobId): ?string
    {
        if ($jobId) {
            return $this->execute([
                'scontrol',
                'show',
                'job',
                $jobId
            ], fn() => 'No info available');
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    public function reloadInfo(): void
    {
        $pendingJobs = app(JobRepository::class)->pendingJobs();

        if ($pendingJobs->count() > 0) {
            $jobIds = $pendingJobs
                ->pluck('id')
                ->toArray();

            $output = $this->execute([
                'sacct',
                '-P',
                '-X',
                '-a',
                '-A'.config('laravel-cluster.connections.slurm.username'),
                '-ojobid,start,end,state',
                '-j',
                implode(',', $jobIds)
            ]);

            $results = collect($this->formatResults($output, [
                'id',
                'queued_at',
                'ended_at',
                'state'
            ]));

            $resultsWithEvents = $this->mapEvents($results);

            $this->triggerEvents($resultsWithEvents);
        }
    }

    /**
     * @inheritDoc
     */
    public function status(): bool
    {
        $output = $this->execute([
            'scontrol',
            'ping'
        ], fn() => '');

        return Str::of($output)->contains('UP');
    }

    /**
     * @inheritDoc
     */
    public function queues(): array
    {
        $username = config('laravel-cluster.connections.slurm.username');

        $output = $this->execute([
            'squeue',
            '--format=%all',
            "-u{$username}"
        ], fn() => []);

        if (empty($output)) {
            return [];
        }

        return $this->formatResults($output);
    }

    /**
     * @inheritDoc
     */
    public function run($job): void
    {
        $job->created_at = Carbon::now();

        // We format the array of commands to a string with newlines between commands
        $content = implode("\n", $job->handle());

        $payload = sprintf("<<EOF
#!/bin/bash

#SBATCH --output=%s
#SBATCH --error=%s
#SBATCH --job-name=%s
#SBATCH --partition=%s
#SBATCH --mem=%s
#SBATCH --time=%s
#SBATCH --account=%s
%s
EOF",
            "{$job->exportPath}/%x_%j.out",
            "{$job->exportPath}/%x_%j.err",
            $job->name,
            $job->partition,
            $job->memory,
            $job->timeout,
            config('laravel-cluster.connections.slurm.username'),
            $content
        );

        $sudoUser = config('laravel-cluster.connections.slurm.sudo_account');

        if (!is_null($sudoUser)) {
            $process = Process::fromShellCommandline("sudo -u $sudoUser sbatch --chdir=/tmp --parsable $payload");
        } else {
            $process = Process::fromShellCommandline("sbatch --chdir=/tmp --parsable $payload");
        }

        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $job->id = (int) $process->getOutput();

        app(JobRepository::class)->store($job, $payload);
    }

    /**
     * @inheritDoc
     */
    public function monitorStatus(): void
    {
        $output = $this->execute([
            'scontrol',
            'ping'
        ], fn() => '');

        if (Str::of($output)->contains('UP')) {
            event(new ClusterUpDetected());
        } else {
            event(new ClusterDownDetected());
        }
    }
}
