<?php

namespace Dterumal\LaravelCluster\Events;

use Dterumal\LaravelCluster\Storage\JobModel;

class JobQueued
{
    /**
     * The queue job instance.
     *
     * @var JobModel
     */
    public JobModel $job;

    /**
     * The datetime of the event.
     *
     * @var string
     */
    public string $datetime;

    /**
     * Create a new event instance.
     *
     * @param  JobModel  $job
     * @param  string  $datetime
     */
    public function __construct(JobModel $job, string $datetime)
    {
        $this->job = $job;
        $this->datetime = $datetime;
    }
}
