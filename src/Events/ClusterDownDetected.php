<?php

namespace Dterumal\LaravelCluster\Events;

use Dterumal\LaravelCluster\Notifications\ClusterDownDetected as ClusterDownDetectedNotification;

class ClusterDownDetected
{
    /**
     * Get a notification representation of the event.
     *
     * @return \Dterumal\LaravelCluster\Notifications\ClusterDownDetected
     */
    public function toNotification()
    {
        return new ClusterDownDetectedNotification();
    }
}
