<?php

namespace Dterumal\LaravelCluster;

trait ClusterDispatchable
{
    /**
     * Dispatch the job with the given arguments.
     *
     * @return PendingClusterDispatch
     */
    public static function dispatch()
    {
        return new PendingClusterDispatch(new static(...func_get_args()));
    }
}
