<?php

namespace Dterumal\LaravelCluster;

use Closure;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use RuntimeException;

class LaravelCluster
{
    /**
     * The callback that should be used to authenticate LaravelCluster users.
     *
     * @var \Closure
     */
    public static $authUsing;

    /**
     * The email address for notifications.
     *
     * @var string
     */
    public static $email;

    /**
     * Determine if the given request can access the LaravelCluster dashboard.
     *
     * @param  Request  $request
     * @return bool
     */
    public static function check(Request $request)
    {
        return (static::$authUsing ?: function () {
            return app()->environment('local');
        })($request);
    }

    /**
     * Set the callback that should be used to authenticate LaravelCluster users.
     *
     * @param  \Closure  $callback
     * @return static
     */
    public static function auth(Closure $callback)
    {
        static::$authUsing = $callback;

        return new static;
    }

    /**
     * Get the default JavaScript variables for LaravelCluster.
     *
     * @return array
     */
    public static function scriptVariables()
    {
        return [
            'path' => config('laravel-cluster.path'),
            'appName' => config('app.name'),
            'assetsAreCurrent' => self::assetsAreCurrent(),
            'isDownForMaintenance' => App::isDownForMaintenance(),
        ];
    }

    /**
     * Specify the email address to which email notifications should be routed.
     *
     * @param  string  $email
     * @return static
     */
    public static function routeMailNotificationsTo($email)
    {
        static::$email = $email;

        return new static;
    }

    /**
     * Determine if LaravelCluster's published assets are up-to-date.
     *
     * @return bool
     *
     * @throws \RuntimeException
     */
    public static function assetsAreCurrent()
    {
        $publishedPath = public_path('vendor/laravel-cluster/mix-manifest.json');

        if (! File::exists($publishedPath)) {
            throw new RuntimeException('LaravelCluster assets are not published. Please run: php artisan laravel-cluster:publish');
        }

        return File::get($publishedPath) === File::get(__DIR__.'/../public/mix-manifest.json');
    }
}
