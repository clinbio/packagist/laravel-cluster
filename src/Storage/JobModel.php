<?php

namespace Dterumal\LaravelCluster\Storage;

use Dterumal\LaravelCluster\Database\Factories\JobModelFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Symfony\Component\Finder\Finder;

class JobModel extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cluster_jobs';

    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'attempts' => 1,
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'runtime'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'job_name',
        'partition',
        'payload',
        'job',
        'std_out_path',
        'std_err_path',
        'export_path',
        'attempts',
        'export',
        'queued_at',
        'failed_at',
        'completed_at',
        'created_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'std_out_path',
        'std_err_path',
        'export_path'
    ];

    /**
     * Get the job payload.
     *
     * @param  string  $value
     * @return \Illuminate\Support\Stringable
     */
    public function getPayloadAttribute($value)
    {
        return Str::of(unserialize($value, ['string']))
            ->remove('<<EOF')
            ->remove('EOF');
    }

    /**
     * Get the job class.
     *
     * @param  string  $value
     */
    public function getJobAttribute($value)
    {
        return unserialize(base64_decode($value), ['allowed_classes' => true]);
    }

    /**
     * Get the runtime.
     *
     * @return float|int|null
     */
    public function getRuntimeAttribute()
    {
        $startDateTime = Carbon::parse($this->queued_at);

        if (isset($this->completed_at)) {
            return $startDateTime->diffInSeconds(Carbon::parse($this->completed_at));
        }

        if (isset($this->failed_at)) {
            return $startDateTime->diffInSeconds(Carbon::parse($this->failed_at));
        }

        return null;
    }

    protected static function newFactory()
    {
        return JobModelFactory::new();
    }
}
