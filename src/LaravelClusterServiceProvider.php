<?php

namespace Dterumal\LaravelCluster;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Facades\Route;

class LaravelClusterServiceProvider extends LaravelClusterApplicationServiceProvider
{
    use EventMap, ServiceBindings;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerEvents();
        $this->registerRoutes();
        $this->registerResources();
        $this->defineAssetPublishing();
        $this->offerPublishing();
        $this->registerCommands();
    }

    /**
     * Register the LaravelCluster job events.
     *
     * @return void
     */
    protected function registerEvents()
    {
        $events = $this->app->make(Dispatcher::class);

        foreach ($this->events as $event => $listeners) {
            foreach ($listeners as $listener) {
                $events->listen($event, $listener);
            }
        }
    }

    /**
     * Register the LaravelCluster routes.
     *
     * @return void
     */
    protected function registerRoutes()
    {
        Route::group([
            'prefix' => config('laravel-cluster.path'),
            'namespace' => 'Dterumal\LaravelCluster\Http\Controllers',
            'middleware' => config('laravel-cluster.middleware', 'web'),
        ], function () {
            $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        });
    }

    /**
     * Register the LaravelCluster resources.
     *
     * @return void
     */
    protected function registerResources()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'laravel-cluster');
    }

    /**
     * Define the asset publishing configuration.
     *
     * @return void
     */
    public function defineAssetPublishing()
    {
        $this->publishes([
            LARAVEL_CLUSTER_PATH.'/public' => public_path('vendor/laravel-cluster'),
        ], 'laravel-cluster-assets');
    }

    /**
     * Setup the resource publishing groups for LaravelCluster.
     *
     * @return void
     */
    protected function offerPublishing()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../stubs/LaravelClusterServiceProvider.stub' => app_path('Providers/LaravelClusterServiceProvider.php'),
            ], 'laravel-cluster-provider');

            $this->publishes([
                __DIR__.'/../config/laravel-cluster.php' => config_path('laravel-cluster.php'),
            ], 'laravel-cluster-config');

            $this->publishes([
                __DIR__ . '/../database/migrations/create_cluster_jobs_table.php.stub' => database_path('migrations/' . date('Y_m_d_His', time()) . '_create_cluster_jobs_table.php'),
                // you can add any number of migrations here
            ], 'laravel-cluster-migrations');
        }
    }

    /**
     * Register the LaravelCluster Artisan commands.
     *
     * @return void
     */
    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                Console\ClusterJobMakeCommand::class,
                Console\ClearCommand::class,
                Console\InstallCommand::class,
                Console\PublishCommand::class,
                Console\RefreshClusterJobsCommand::class,
                Console\MonitorClusterStatusCommand::class,
                Console\TrimCommand::class,
            ]);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (! defined('LARAVEL_CLUSTER_PATH')) {
            define('LARAVEL_CLUSTER_PATH', realpath(__DIR__.'/../'));
        }

        $this->configure();
        $this->registerServices();
    }

    /**
     * Setup the configuration for LaravelCluster.
     *
     * @return void
     */
    protected function configure()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/laravel-cluster.php', 'laravel-cluster'
        );
    }

    /**
     * Register LaravelCluster's services in the container.
     *
     * @return void
     */
    protected function registerServices()
    {
        foreach ($this->serviceBindings as $key => $value) {
            is_numeric($key)
                ? $this->app->singleton($value)
                : $this->app->singleton($key, $value);
        }
    }
}
