<?php

namespace Dterumal\LaravelCluster;

trait EventMap
{
    /**
     * All of the LaravelCluster event / listener mappings.
     *
     * @var array
     */
    protected $events = [
        Events\JobCancelled::class => [
            Listeners\ExecuteJobCancelCallback::class,
            Listeners\MarkJobAsCancelled::class
        ],

        Events\JobFailed::class => [
            Listeners\ExecuteJobFailureCallback::class,
            Listeners\MarkJobAsFailed::class
        ],

        Events\JobCompleted::class => [
            Listeners\ExecuteJobSuccessCallback::class,
            Listeners\MarkJobAsCompleted::class
        ],

        Events\JobQueued::class => [
            Listeners\MarkJobAsQueued::class
        ],

        Events\ClusterUpDetected::class => [
            Listeners\ResetClusterStatus::class
        ],

        Events\ClusterDownDetected::class => [
            Listeners\SendNotification::class,
            Listeners\StoreClusterDown::class
        ]
    ];
}
