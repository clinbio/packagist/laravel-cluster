<?php


namespace Dterumal\LaravelCluster;


use Illuminate\Support\Facades\Gate;

class LaravelClusterApplicationServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->authorization();
    }

    /**
     * Configure the LaravelCluster authorization services.
     *
     * @return void
     */
    protected function authorization()
    {
        $this->gate();

        LaravelCluster::auth(function ($request) {
            return app()->environment('local') ||
                Gate::check('viewLaravelCluster', [$request->user()]);
        });
    }

    /**
     * Register the LaravelCluster gate.
     *
     * This gate determines who can access LaravelCluster in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewLaravelCluster', function ($user) {
            return in_array($user->email, [
                //
            ], true);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
