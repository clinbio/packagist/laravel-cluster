<?php


namespace Dterumal\LaravelCluster\Repositories;


use Dterumal\LaravelCluster\Contracts\MetricsRepository;
use Dterumal\LaravelCluster\Storage\JobModel;
use Illuminate\Support\Carbon;

class DatabaseMetricsRepository implements MetricsRepository
{

    /**
     * Returns total of jobs
     *
     * @return int
     */
    public function count(): int
    {
        return JobModel::query()->count();
    }

    /**
     * Returns total of jobs of last hour
     *
     * @return int
     */
    public function countLastHour(): int
    {
        return JobModel::whereDate('queued_at', Carbon::now()->format('Y-m-d'))
            ->whereTime('queued_at', '>', Carbon::now()->subHour()->format('H:i:s'))
            ->count();
    }

    /**
     * Returns total of failed jobs
     *
     * @return int
     */
    public function countFailed(): int
    {
        return JobModel::whereNotNull('failed_at')
            ->count();
    }
}
