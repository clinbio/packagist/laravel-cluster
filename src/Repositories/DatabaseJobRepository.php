<?php


namespace Dterumal\LaravelCluster\Repositories;


use Dterumal\LaravelCluster\Contracts\JobRepository;
use Dterumal\LaravelCluster\Storage\JobModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Carbon;

class DatabaseJobRepository implements JobRepository
{

    /**
     * Store the hob into storage
     *
     * @param $job
     * @param  string  $payload
     * @return JobModel
     */
    public function store($job, string $payload): JobModel
    {
        return JobModel::forceCreate([
            'id' => $job->id,
            'job_name' => $job->name,
            'partition' => $job->partition,
            'job' => base64_encode(serialize($job)),
            'payload' => serialize($payload),
            'std_out_path' => "{$job->exportPath}/{$job->name}_{$job->id}.out",
            'std_err_path' => "{$job->exportPath}/{$job->name}_{$job->id}.err",
            'export_path' => $job->exportPath,
            'created_at' => $job->created_at
        ]);
    }

    /**
     * Mark the job as queued
     *
     * @param  JobModel  $clusterJob
     * @param  string  $datetime
     * @return void
     */
    public function queued(JobModel $clusterJob, string $datetime): void
    {
        if ($datetime !== 'Unknown') {
            $clusterJob->update([
                'queued_at' => $datetime
            ]);
        }
    }

    /**
     * Mark the job as failed
     *
     * @param  JobModel  $clusterJob
     * @param  string  $datetime
     * @return void
     */
    public function failed(JobModel $clusterJob, string $datetime): void
    {
        $clusterJob->update([
            'failed_at' => $datetime
        ]);
    }

    /**
     * Mark the job as cancelled
     *
     * @param  JobModel  $clusterJob
     * @param  string  $datetime
     * @return void
     */
    public function cancelled(JobModel $clusterJob, string $datetime): void
    {
        $clusterJob->update([
            'cancelled_at' => $datetime
        ]);
    }

    /**
     * Mark the job as completed
     *
     * @param  JobModel  $clusterJob
     * @param  string  $datetime
     * @return void
     */
    public function completed(JobModel $clusterJob, string $datetime): void
    {
        $clusterJob->update([
            'completed_at' => $datetime
        ]);
    }

    /**
     * Returns all the pending jobs
     *
     * @return Collection
     */
    public function pendingJobs(): Collection
    {
        return JobModel::whereNull(['cancelled_at', 'failed_at', 'completed_at'])
            ->orderBy('id', 'desc')
            ->get();
    }

    /**
     * Returns all the completed jobs
     *
     * @return Collection
     */
    public function completedJobs(): Collection
    {
        return JobModel::whereNotNull('completed_at')
            ->orderBy('id', 'desc')
            ->get();
    }

    /**
     * Returns all the failed jobs
     *
     * @return Collection
     */
    public function failedJobs(): Collection
    {
        return JobModel::whereNotNull('failed_at')
            ->orderBy('id', 'desc')
            ->get();
    }

    /**
     * Returns all the failed jobs
     *
     * @return Collection
     */
    public function cancelledJobs(): Collection
    {
        return JobModel::whereNotNull('cancelled_at')
            ->orderBy('id', 'desc')
            ->get();
    }

    /**
     * Returns a specific cluster job
     *
     * @param  JobModel  $clusterJob
     * @return JobModel
     */
    public function find(JobModel $clusterJob): JobModel
    {
        return $clusterJob;
    }

    /**
     * Delete pending and created jobs
     *
     * @return int
     */
    public function purge(): int
    {
        $ids = $this->pendingJobs()->pluck('id');

        $count = count($ids);

        JobModel::destroy($ids);

        return $count;
    }

    /**
     * Trim jobs based on configured minutes.
     *
     * @return void
     */
    public function trim(): void
    {
        $completedJobs = $this->completedJobs()->filter(function ($job) {
            return Carbon::parse($job->completed_at)->diffInMinutes(now()) > config('laravel-cluster.trim.completed');
        })->pluck('id');

        JobModel::destroy($completedJobs);

        $pendingJobs = $this->pendingJobs()->filter(function ($job) {
            return Carbon::parse($job->queued_at)->diffInMinutes(now()) > config('laravel-cluster.trim.pending');
        })->pluck('id');

        JobModel::destroy($pendingJobs);

        $failedJobs = $this->failedJobs()->filter(function ($job) {
            return Carbon::parse($job->failed_at)->diffInMinutes(now()) > config('laravel-cluster.trim.failed');
        })->pluck('id');

        JobModel::destroy($failedJobs);
    }
}
