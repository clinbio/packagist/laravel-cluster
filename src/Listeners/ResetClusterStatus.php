<?php

namespace Dterumal\LaravelCluster\Listeners;

use Dterumal\LaravelCluster\Events\ClusterUpDetected;
use Illuminate\Contracts\Cache\Repository as Cache;

class ResetClusterStatus
{
    /**
     * The cache store implementation.
     *
     * @var \Illuminate\Contracts\Cache\Repository
     */
    protected $cache;

    /**
     * Create a new queue restart command.
     *
     * @param  \Illuminate\Contracts\Cache\Repository  $cache
     * @return void
     */
    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Handle the event.
     *
     * @param  \Dterumal\LaravelCluster\Events\ClusterUpDetected  $event
     * @return void
     */
    public function handle(ClusterUpDetected $event): void
    {
        $this->cache->forget('laravel-cluster:status:down');
    }
}
