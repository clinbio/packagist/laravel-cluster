<?php

namespace Dterumal\LaravelCluster\Listeners;

use Dterumal\LaravelCluster\Events\ClusterDownDetected;
use Dterumal\LaravelCluster\LaravelCluster;
use Illuminate\Support\Facades\Notification;
use Illuminate\Contracts\Cache\Repository as Cache;

class SendNotification
{
    /**
     * The cache store implementation.
     *
     * @var \Illuminate\Contracts\Cache\Repository
     */
    protected Cache $cache;

    /**
     * Create a new queue restart command.
     *
     * @param  \Illuminate\Contracts\Cache\Repository  $cache
     * @return void
     */
    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Handle the event.
     *
     * @param  \Dterumal\LaravelCluster\Events\ClusterDownDetected  $event
     * @return void
     */
    public function handle(ClusterDownDetected $event): void
    {
        $notification = $event->toNotification();

        if (!$this->cache->has('laravel-cluster:status:down')) {
            Notification::route('mail', LaravelCluster::$email)
                ->notify($notification);
        }
    }
}
