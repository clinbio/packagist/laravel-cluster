<?php

namespace Dterumal\LaravelCluster\Listeners;

use Dterumal\LaravelCluster\Contracts\JobRepository;
use Dterumal\LaravelCluster\Events\JobQueued;

class MarkJobAsQueued
{
    /**
     * The job repository implementation.
     *
     * @var \Dterumal\LaravelCluster\Contracts\JobRepository
     */
    public JobRepository $jobs;

    /**
     * Create a new listener instance.
     *
     * @param  \Dterumal\LaravelCluster\Contracts\JobRepository  $jobs
     * @return void
     */
    public function __construct(JobRepository $jobs)
    {
        $this->jobs = $jobs;
    }

    /**
     * Handle the event.
     *
     * @param  \Dterumal\LaravelCluster\Events\JobQueued  $event
     * @return void
     */
    public function handle(JobQueued $event): void
    {
        $this->jobs->queued($event->job, $event->datetime);

    }
}
