<?php

namespace Dterumal\LaravelCluster\Listeners;

use Dterumal\LaravelCluster\Contracts\JobRepository;
use Dterumal\LaravelCluster\Events\JobFailed;

class MarkJobAsFailed
{
    /**
     * The job repository implementation.
     *
     * @var \Dterumal\LaravelCluster\Contracts\JobRepository
     */
    public JobRepository $jobs;

    /**
     * Create a new listener instance.
     *
     * @param  \Dterumal\LaravelCluster\Contracts\JobRepository  $jobs
     * @return void
     */
    public function __construct(JobRepository $jobs)
    {
        $this->jobs = $jobs;
    }

    /**
     * Handle the event.
     *
     * @param  \Dterumal\LaravelCluster\Events\JobFailed  $event
     * @return void
     */
    public function handle(JobFailed $event): void
    {
        $this->jobs->failed($event->job, $event->datetime);

    }
}
