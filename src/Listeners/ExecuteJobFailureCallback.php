<?php

namespace Dterumal\LaravelCluster\Listeners;

use Dterumal\LaravelCluster\Events\JobFailed;

class ExecuteJobFailureCallback
{
    /**
     * Handle the event.
     *
     * @param  \Dterumal\LaravelCluster\Events\JobFailed  $event
     * @return void
     */
    public function handle(JobFailed $event): void
    {
        $jobClass = $event->job->job;

        if (method_exists($jobClass, 'onFailure')) {
            $jobClass->onFailure($event->job);
        }
    }
}
