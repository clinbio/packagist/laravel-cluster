<?php

namespace Dterumal\LaravelCluster\Listeners;

use Dterumal\LaravelCluster\Events\JobCancelled;

class ExecuteJobCancelCallback
{
    /**
     * Handle the event.
     *
     * @param  \Dterumal\LaravelCluster\Events\JobCancelled  $event
     * @return void
     */
    public function handle(JobCancelled $event): void
    {
        $jobClass = $event->job->job;

        if (method_exists($jobClass, 'onCancel')) {
            $jobClass->onCancel($event->job);
        }
    }
}
