<?php

namespace Dterumal\LaravelCluster\Listeners;

use Dterumal\LaravelCluster\Events\ClusterDownDetected;
use Illuminate\Contracts\Cache\Repository as Cache;
use Illuminate\Support\InteractsWithTime;

class StoreClusterDown
{
    use InteractsWithTime;
    /**
     * The cache store implementation.
     *
     * @var \Illuminate\Contracts\Cache\Repository
     */
    protected Cache $cache;

    /**
     * Create a new queue restart command.
     *
     * @param  \Illuminate\Contracts\Cache\Repository  $cache
     * @return void
     */
    public function __construct(Cache $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Handle the event.
     *
     * @param  \Dterumal\LaravelCluster\Events\ClusterDownDetected  $event
     * @return void
     */
    public function handle(ClusterDownDetected $event): void
    {
        $this->cache->forever('laravel-cluster:status:down', $this->currentTime());
    }
}
