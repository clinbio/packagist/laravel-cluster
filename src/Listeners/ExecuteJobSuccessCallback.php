<?php

namespace Dterumal\LaravelCluster\Listeners;

use Dterumal\LaravelCluster\Events\JobCompleted;

class ExecuteJobSuccessCallback
{
    /**
     * Handle the event.
     *
     * @param  \Dterumal\LaravelCluster\Events\JobCompleted  $event
     * @return void
     */
    public function handle(JobCompleted $event): void
    {
        $jobClass = $event->job->job;

        if (method_exists($jobClass, 'onSuccess')) {
            $jobClass->onSuccess($event->job);
        }
    }
}
