<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Laravel Cluster Path
    |--------------------------------------------------------------------------
    |
    | This is the URI path where LaravelCluster will be accessible from. Feel free
    | to change this path to anything you like. Note that the URI will not
    | affect the paths of its internal API that aren't exposed to users.
    |
    */

    'path' => env('LARAVEL_CLUSTER_PATH', 'laravel-cluster'),

    /*
    |--------------------------------------------------------------------------
    | LaravelCluster Route Middleware
    |--------------------------------------------------------------------------
    |
    | These middleware will get attached onto each LaravelCluster route, giving you
    | the chance to add your own middleware to this list or change any of
    | the existing middleware. Or, you can simply stick with this list.
    |
    */

    'middleware' => ['web'],

    /*
    |--------------------------------------------------------------------------
    | Default Cluster Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the cluster connections below you wish
    | to use as your default connection for all jobs computation.
    |
    */

    'default' => env('CLUSTER_CONNECTION', 'slurm'),

    /*
    |--------------------------------------------------------------------------
    | Cluster Settings
    |--------------------------------------------------------------------------
    |
    | Here you may configure the cluster connection information. A default
    | configuration has been configured when installing this package.
    | You are free to add more.
    |
    | Drivers: "slurm", "null"
    |
    */

    'connections' => [

        'slurm' => [
            'driver' => 'slurm',
            'username' => env('CLUSTER_USERNAME'),
            'sudo_account' => env('CLUSTER_SUDO_ACCOUNT')
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Job Trimming Times
    |--------------------------------------------------------------------------
    |
    | Here you can configure for how long (in minutes) you desire LaravelCluster to
    | persist the recent and failed jobs. Typically, recent jobs are kept
    | for one hour while all failed jobs are stored for an entire week.
    |
    */

    'trim' => [
        'pending' => 60,
        'completed' => 60,
        'failed' => 10080,
    ],

    /*
    |--------------------------------------------------------------------------
    | Default job name
    |--------------------------------------------------------------------------
    */

    'name' => 'default',

    /*
    |--------------------------------------------------------------------------
    | Default partition
    |--------------------------------------------------------------------------
    */

    'partition' => 'normal',

    /*
    |--------------------------------------------------------------------------
    | Timeout Limit
    |--------------------------------------------------------------------------
    */

    'timeout_limit' => '00:01:00',

    /*
    |--------------------------------------------------------------------------
    | Memory Limit
    |--------------------------------------------------------------------------
    */

    'memory_limit' => '1G',

];
