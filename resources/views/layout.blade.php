<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('/vendor/laravel-cluster/img/favicon.ico') }}">


    <title>LaravelCluster{{ config('app.name') ? ' - ' . config('app.name') : '' }}</title>

    <title>{{ Config::get('app.name') }}</title>
</head>
<body>
    <div id="laravel-cluster" v-cloak></div>

    <!-- Global LaravelCluster Object -->
    <script>
        window.LaravelCluster = @json($laravelClusterScriptVariables);
    </script>

    <script src="{{asset(mix('app.js', 'vendor/laravel-cluster'))}}"></script>
</body>
</html>
