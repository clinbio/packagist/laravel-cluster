import axios from 'axios'

declare module 'vue/types/vue' {
    export interface Vue {
        $http: typeof axios
    }
}
