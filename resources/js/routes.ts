import {RouteConfig} from "vue-router";
import Dashboard from "@/Pages/Dashboard.vue";
import Inspector from "@/Pages/Inspector.vue";
import Queue from "@/Pages/Queue.vue";
import PendingJobs from "@/Pages/PendingJobs.vue";
import ShowPendingJob from "@/Pages/ShowPendingJob.vue";
import CompletedJobs from "@/Pages/CompletedJobs.vue";
import ShowCompletedJob from "@/Pages/ShowCompletedJob.vue";
import FailedJobs from "@/Pages/FailedJobs.vue";
import ShowFailedJob from "@/Pages/ShowFailedJob.vue";
import CancelledJobs from "@/Pages/CancelledJobs.vue";
import ShowCancelledJob from "@/Pages/ShowCancelledJob.vue";

const routes: RouteConfig[] = [
    {
        path: '/',
        redirect: '/dashboard'
    },

    {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard
    },

    {
        path: '/inspector',
        name: 'inspector',
        component: Inspector
    },

    {
        path: '/queue',
        name: 'queue',
        component: Queue
    },

    {
        path: '/pending',
        name: 'pending-jobs',
        component: PendingJobs
    },

    {
        path: '/pending/:id',
        name: 'pending-jobs-preview',
        component: ShowPendingJob
    },

    {
        path: '/completed',
        name: 'completed-jobs',
        component: CompletedJobs
    },

    {
        path: '/completed/:id',
        name: 'completed-jobs-preview',
        component: ShowCompletedJob
    },

    {
        path: '/failed',
        name: 'failed-jobs',
        component: FailedJobs
    },

    {
        path: '/failed/:id',
        name: 'failed-jobs-preview',
        component: ShowFailedJob
    },

    {
        path: '/cancelled',
        name: 'cancelled-jobs',
        component: CancelledJobs
    },

    {
        path: '/cancelled/:id',
        name: 'cancelled-jobs-preview',
        component: ShowCancelledJob
    },
]

export default routes
