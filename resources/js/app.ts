import Vue from 'vue'
import vuetify from './vuetify';
import axios from "axios";
import Routes from './routes'
import VueRouter from "vue-router";
import LaravelCluster from "@/LaravelCluster.vue";

let token: any = document.head.querySelector('meta[name="csrf-token"]');

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

if (token) {
    axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
}

Vue.use(VueRouter);

Vue.prototype.$http = axios.create({
    timeout: 5000
});

declare global {
    interface Window {
        LaravelCluster: any
    }
}

window.LaravelCluster.basePath = '/' + window.LaravelCluster.path;

let routerBasePath = window.LaravelCluster.basePath + '/';

if (window.LaravelCluster.path === '' || window.LaravelCluster.path === '/') {
    routerBasePath = '/';
    window.LaravelCluster.basePath = '';
}

const router = new VueRouter({
    routes: Routes,
    mode: 'history',
    base: routerBasePath,
});

const el: HTMLElement = document.getElementById('laravel-cluster') || document.createElement('laravel-cluster')

new Vue({
    router,
    vuetify,
    render: h => h(LaravelCluster)
}).$mount(el)
