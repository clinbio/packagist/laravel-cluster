<?php

namespace Dterumal\LaravelCluster\Tests;

use Illuminate\Database\Eloquent\Factories\Factory;
use Orchestra\Testbench\TestCase as Orchestra;
use Dterumal\LaravelCluster\LaravelClusterServiceProvider;

class TestCase extends Orchestra
{
    public function setUp(): void
    {
        parent::setUp();

        Factory::guessFactoryNamesUsing(
            fn (string $modelName) => 'Dterumal\\LaravelCluster\\Database\\Factories\\'.class_basename($modelName).'Factory'
        );
    }

    protected function getPackageProviders($app)
    {
        return [
            LaravelClusterServiceProvider::class,
        ];
    }

    public function getEnvironmentSetUp($app)
    {
        config()->set('database.default', 'testing');

        // import the CreatePostsTable class from the migration
        include_once __DIR__ . '/../database/migrations/create_cluster_jobs_table.php.stub';

        // run the up() method of that migration class
        (new \CreateClusterJobsTable)->up();
    }
}
