<?php


namespace Dterumal\LaravelCluster\Tests\Unit;


use Dterumal\LaravelCluster\Storage\JobModel;
use Dterumal\LaravelCluster\Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;

class JobModelTest extends TestCase
{
    use RefreshDatabase;

    function test_a_job_has_an_id()
    {
        $clusterJob = JobModel::factory()->create(['id' => 12345]);
        $this->assertEquals(12345, $clusterJob->id);
    }

    function test_a_job_has_a_name()
    {
        $clusterJob = JobModel::factory()->create(['job_name' => 'Fake name']);
        $this->assertEquals('Fake name', $clusterJob->job_name);
    }

    function test_a_job_has_a_partition()
    {
        $clusterJob = JobModel::factory()->create(['partition' => 'Fake partition']);
        $this->assertEquals('Fake partition', $clusterJob->partition);
    }

    function test_a_job_has_a_payload()
    {
        $payload = "<<EOF
#!/bin/bash

#SBATCH --output=/tmp/standard.out
#SBATCH --error=/tmp/standard.err
#SBATCH --job-name=default
#SBATCH --partition=normal
#SBATCH --mem=1G
#SBATCH --time=00:01:00

echo Hello World!

EOF";
        $clusterJob = JobModel::factory()->create(['payload' => serialize($payload)]);
        $this->assertEquals($payload, unserialize($clusterJob->getRawOriginal('payload')));
    }

    function test_a_job_has_a_class()
    {
        $class = 'O:18:"App\Jobs\SimpleJob":5:{s:7:"message";s:6:"World!";s:4:"name";s:7:"default";s:9:"partition";s:6:"normal";s:6:"memory";s:2:"1G";s:7:"timeout";s:8:"00:01:00";}';

        $clusterJob = JobModel::factory()->create(['job' => base64_encode(serialize($class))]);
        $this->assertEquals($class, unserialize(base64_decode($clusterJob->getRawOriginal('job'))));
   }

    function test_a_job_has_a_std_out_path()
    {
        $clusterJob = JobModel::factory()->create(['std_out_path' => '/path/to/file']);
        $this->assertEquals('/path/to/file', $clusterJob->std_out_path);
    }

    function test_a_job_has_a_std_err_path()
    {
        $clusterJob = JobModel::factory()->create(['std_err_path' => '/path/to/file']);
        $this->assertEquals('/path/to/file', $clusterJob->std_err_path);
    }

    function test_a_job_has_a_default_created_at()
    {
        $clusterJob = JobModel::factory()->create();
        $this->assertNotNull($clusterJob->created_at);
    }

    function test_a_job_has_a_queued_at()
    {
        $now = Carbon::now();
        $clusterJob = JobModel::factory()->create(['queued_at' => $now]);
        $this->assertEquals($now, $clusterJob->queued_at);
    }

    function test_a_job_has_a_null_cancelled_at()
    {
        $clusterJob = JobModel::factory()->create();
        $this->assertEquals(null, $clusterJob->cancelled_at);
    }

    function test_a_job_has_a_null_failed_at()
    {
        $clusterJob = JobModel::factory()->create();
        $this->assertEquals(null, $clusterJob->failed_at);
    }

    function test_a_job_has_a_null_completed_at()
    {
        $clusterJob = JobModel::factory()->create();
        $this->assertEquals(null, $clusterJob->completed_at);
    }
}
