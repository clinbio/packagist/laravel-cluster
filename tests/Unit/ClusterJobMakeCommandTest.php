<?php

namespace Dterumal\LaravelCluster\Tests\Unit;

use Dterumal\LaravelCluster\Tests\TestCase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class ClusterJobMakeCommandTest extends TestCase
{
    function test_it_creates_a_new_cluster_job()
    {
        // destination path of the Foo class
        $fooClusterJob = app_path('Jobs/MyFooClusterJob.php');

        // make sure we're starting from a clean state
        if (File::exists($fooClusterJob)) {
            unlink($fooClusterJob);
        }

        $this->assertFalse(File::exists($fooClusterJob));

        // Run the make command
        Artisan::call('make:cluster-job MyFooClusterJob');

        // Assert a new file is created
        $this->assertTrue(File::exists($fooClusterJob));

        // Assert the file contains the right contents
        $expectedContents = <<<CLASS
<?php

namespace App\Jobs;

use Dterumal\LaravelCluster\ClusterDispatchable;
use Dterumal\LaravelCluster\ClusterQueueable;
use Dterumal\LaravelCluster\Contracts\ClusterShouldQueue;

class MyFooClusterJob implements ClusterShouldQueue
{
    use ClusterQueueable, ClusterDispatchable;

    /**
     * Create a new job instance.
     *
     * @return  void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job defined by a list of commands
     *
     @return array
     */
    public function handle(): array
    {
        return [
            //
        ];
    }
}

CLASS;

        $this->assertEquals($expectedContents, file_get_contents($fooClusterJob));

    }

    function test_it_creates_a_new_cluster_job_with_callback()
    {
        // destination path of the Foo class
        $fooClusterJob = app_path('Jobs/MyFooClusterJob.php');

        // make sure we're starting from a clean state
        if (File::exists($fooClusterJob)) {
            unlink($fooClusterJob);
        }

        $this->assertFalse(File::exists($fooClusterJob));

        // Run the make command
        Artisan::call('make:cluster-job MyFooClusterJob --callback');

        // Assert a new file is created
        $this->assertTrue(File::exists($fooClusterJob));

        // Assert the file contains the right contents
        $expectedContents = <<<CLASS
<?php

namespace App\Jobs;

use Dterumal\LaravelCluster\ClusterDispatchable;
use Dterumal\LaravelCluster\ClusterQueueable;
use Dterumal\LaravelCluster\Contracts\ClusterShouldQueue;
use Dterumal\LaravelCluster\Contracts\ClusterWithCallbacks;
use Dterumal\LaravelCluster\Storage\JobModel;

class MyFooClusterJob implements ClusterShouldQueue, ClusterWithCallbacks
{
    use ClusterQueueable, ClusterDispatchable;

    /**
     * Create a new job instance.
     *
     * @return  void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job defined by a list of commands
     *
     @return array
     */
    public function handle(): array
    {
        return [
            //
        ];
    }

    /**
     * Execute the following callback when the job fails
     *
     * @param \Dterumal\LaravelCluster\Storage\JobModel \$job
     * @param \Throwable \$e
     * @return void
     */
    public function onFailure(JobModel \$job, \Throwable \$e): void
    {
        //
    }

    /**
     * Execute the following callback when the job successfully
     * completes
     *
     * @param \Dterumal\LaravelCluster\Storage\JobModel \$job
     * @return void
     */
    public function onSuccess(JobModel \$job): void
    {
        //
    }

    /**
     * Execute the following callback when the job is cancelled
     *
     * @param \Dterumal\LaravelCluster\Storage\JobModel \$job
     * @return void
     */
    public function onCancel(JobModel \$job): void
    {
        //
    }
}

CLASS;

        $this->assertEquals($expectedContents, file_get_contents($fooClusterJob));

    }
}
