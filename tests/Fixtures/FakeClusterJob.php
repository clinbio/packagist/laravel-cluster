<?php

namespace Dterumal\LaravelCluster\Tests\Fixtures;

use Dterumal\LaravelCluster\ClusterDispatchable;
use Dterumal\LaravelCluster\ClusterQueueable;
use Dterumal\LaravelCluster\Contracts\ClusterShouldQueue;

class FakeClusterJob implements ClusterShouldQueue
{
    use ClusterQueueable, ClusterDispatchable;

    public string $message;

    /**
     * Create a new job instance.
     *
     * @param  string  $message
     */
    public function __construct(string $message)
    {
        $this->message = $message;
    }

    /**
     * Execute the job defined by a list of commands
     *
     @return array
     */
    public function handle(): array
    {
        return [
            "echo {$this->message}"
        ];
    }
}
