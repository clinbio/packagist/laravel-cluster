# Changelog

All notable changes to `laravel-cluster` will be documented in this file.

## 1.0.0 - 2021-06-30

- Initial release
