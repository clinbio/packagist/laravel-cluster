<p align="center"><img src="https://banners.beyondco.de/Laravel%20Cluster.png?theme=light&packageManager=composer+require&packageName=dterumal%2Flaravel-cluster&pattern=architect&style=style_1&description=Unified+queueing+API+and+dashboard+for+cluster+systems&md=1&showWatermark=1&fontSize=100px&images=chip"></p>

## Introduction

Laravel cluster provides a beautiful dashboard and code-driven configuration for your Laravel powered cluster. Larvel cluster allows you to easily monitor key metrics of your cluster system such as job throughput, runtime, and job failures.

All of your cluster configuration is stored in a single, simple configuration file, allowing your configuration to stay in source control where your entire team can collaborate.

## Installation

You can install the package via composer:

```bash
composer config repositories.gitlab.sib.swiss/519 '{"type": "composer", "url": "https://gitlab.sib.swiss/api/v4/group/519/-/packages/composer/packages.json"}'
composer require dterumal/laravel-cluster
```

After installing Horizon, publish its assets using the `laravel-cluster:install` Artisan command:

```bash
php artisan laravel-cluster:install
```

### Configuration

After publishing Laravel cluster's assets, its primary configuration file will be located at `config/laravel-cluster.php`. This configuration file allows you to configure the queue worker options for your application. Each configuration option includes a description of its purpose, so be sure to thoroughly explore this file.

### Migrations

You should also find a migration file `database/migrations/xxxx_xx_xx_xxxxxx_create_cluster_jobs_table` which will create a table to store your jobs information. Make sure that your application is using a database.

### Dashboard Authorization

Laravel cluster exposes a dashboard at the `/laravel-cluster` URI. By default, you will only be able to access this dashboard in the local environment. However, within your app/Providers/LaravelClusterServiceProvider.php file, there is an authorization gate definition. This authorization gate controls access to Laravel cluster in non-local environments. You are free to modify this gate as needed to restrict access to your Laravel cluster installation:

```php
/**
* Register the LaravelCluster gate.
*
* This gate determines who can access LaravelCluster in non-local environments.
*
* @return void
  */
  protected function gate()
  {
      Gate::define('viewLaravelCluster', function ($user) {
          return in_array($user->email, [
            'dillenn.terumalai@sib.swiss',
          ], true);
      });
  }
```

### Cluster live information

Laravel cluster includes a metrics dashboard which provides information regarding your jobs. In order to keep this dashboard up to date, you should configure Laravel cluster's `refresh` Artisan command to run every minute via your application's scheduler:

```php
/**
 * Define the application's command schedule.
 *
 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
 * @return void
 */
protected function schedule(Schedule $schedule)
{
    $schedule->command('laravel-cluster:refresh')->everyMinute();
}
```

### Monitor cluster status

You can easily monitor the cluster status by configure Laravel cluster's `monitor` Artisan command to run every 5 minutes via your application's scheduler:

```php
/**
 * Define the application's command schedule.
 *
 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
 * @return void
 */
protected function schedule(Schedule $schedule)
{
    $schedule->command('laravel-cluster:monitor')->everyFiveMinutes();
}
```


### Notifications

If you would like to be notified when the cluster is unreachable/down, you may use the LaravelCluster::routeMailNotificationsTo method. You may call this method from the boot method of your application's App\Providers\LaravelClusterServiceProvider:

```php
/**
 * Bootstrap any application services.
 *
 * @return void
 */
public function boot()
{
    parent::boot();

    LaravelCluster::routeMailNotificationsTo('example@example.com');
}
```

### Clearing jobs from queue

If you would like to delete all pending jobs from the cluster's queue, you may do so using the `laravel-cluster:clear` Artisan command:

```bash
php artisan horizon:clear
```

## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](.gitlab/SECURITY.md) on how to report security vulnerabilities.

## Credits

This package is highly inspired from [Laravel Horizon](https://github.com/laravel/horizon).

- [Dillenn Terumalai](https://gitlab.sib.swiss/dterumal)
- [Spatie/Package-Skeleton-Laravel](https://github.com/spatie/package-skeleton-laravel)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
