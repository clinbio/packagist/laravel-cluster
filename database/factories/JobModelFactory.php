<?php

namespace Dterumal\LaravelCluster\Database\Factories;

use Carbon\Carbon;
use Dterumal\LaravelCluster\Storage\JobModel;
use Illuminate\Database\Eloquent\Factories\Factory;

class JobModelFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = JobModel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => $this->faker->numerify('#####'),
            'job_name' => 'default',
            'partition' => 'normal',
            'payload' => serialize('random string'),
            'job' => base64_encode(serialize('random string')),
            'std_out_path' => '/path/to/std/out',
            'std_err_path' => '/path/to/std/err',
            'export_path' => null,
            'created_at' => Carbon::now()
        ];
    }
}
